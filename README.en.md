# XiaoMaYi-EleVue

#### Description
小蚂蚁云单体Vue3+ElementPlus前后端分离后台管理系统，基于SpringBoot3、Vue3、TypeScript、ElementPlus、MySQL等技术栈实现；支持多主题切换模式，多端兼容手机客户端、PAD平板、PC电脑等终端设备，为了高度适配企业、政府和开发者定制化项目的需求，目前单体前后端分离架构发行了多个版本按需选择即可，已集成权限架构体系和基础模块，开发效率，节省人力。

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

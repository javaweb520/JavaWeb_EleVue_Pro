// +----------------------------------------------------------------------
// | 小蚂蚁云企业级开发框架 [ 赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | 版权所有 2022~2024 小蚂蚁团队
// +----------------------------------------------------------------------
// | Licensed Apache-2.0 【小蚂蚁云】并不是自由软件，未经许可禁止去掉相关版权
// +----------------------------------------------------------------------
// | 官方网站: https://www.xiaomayicloud.com
// +----------------------------------------------------------------------
// | 软件作者: @小蚂蚁团队 团队荣誉出品
// +----------------------------------------------------------------------
// | 版权和免责声明:
// | 本团队对该软件框架产品拥有知识产权（包括但不限于商标权、专利权、著作权、商业秘密等）
// | 均受到相关法律法规的保护，任何个人、组织和单位不得在未经本团队书面授权的情况下对所授权
// | 软件框架产品本身申请相关的知识产权，被授权主体务必妥善保管官方所授权的软件产品源码，禁
// | 止以任何形式对外泄露(包括但不限于分享、开源、网络平台),禁止用于任何违法、侵害他人合法
// | 权益等恶意的行为，禁止用于任何违反我国法律法规的一切项目研发，任何个人、组织和单位用于
// | 项目研发而产生的任何意外、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括
// | 但不限于直接、间接、附带或衍生的损失等)，本团队不承担任何法律责任，本软件框架禁止任何
// | 单位、组织、个人用于任何违法、侵害他人合法利益等恶意的行为，如有发现违规、违法的犯罪行
// | 为，本团队将无条件配合公安机关调查取证同时保留一切以法律手段起诉的权利，本软件框架只能
// | 用于公司和个人内部的法律所允许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声
// | 明》附件；
// +----------------------------------------------------------------------

package ${packageName}.dto.${entity?lower_case};

<#if fileComment?contains("分页")>
import com.xiaomayi.mybatis.dto.PageDTO;
</#if>
<#if springdoc>
import io.swagger.v3.oas.annotations.media.Schema;
<#elseif swagger>
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
</#if>
<#if entityLombokModel>
import lombok.Data;
</#if>
<#if fileComment?contains("分页")>
import lombok.EqualsAndHashCode;
</#if>
<#if fileComment?contains("添加") || fileComment?contains("更新")>
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
</#if>

import java.time.LocalDateTime;

/**
 * <p>
 * ${table.comment!}
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
<#if entityLombokModel>
@Data
</#if>
<#if fileComment?contains("分页")>
@EqualsAndHashCode(callSuper = true)
</#if>
<#if springdoc>
@Schema(name = "${table.comment!}${fileComment}", description = "${table.comment!}${fileComment}")
<#elseif swagger>
@ApiModel(value = "${table.comment!}", description = "${table.comment!}")
</#if>
<#if superEntityClass??>
public class ${entity} extends ${superEntityClass}<#if activeRecord><${entity}></#if> {
<#elseif activeRecord>
public class ${entity} extends Model<${entity}> {
<#elseif entitySerialVersionUID>
public class ${entity} implements Serializable {
<#else>
<#if fileComment?contains("分页")>
public class ${entity}${modelSuffix} extends PageDTO {
<#else>
public class ${entity}${modelSuffix} {
</#if>
</#if>
<#if fileComment?contains("分页") || fileComment?contains("列表")>
<#-- ----------  BEGIN 字段循环遍历  ---------->
<#list table.fields as field>
    <#if field.comment?contains('名称') || field.comment?contains('标题') || field.comment?contains('状态') || field.comment?contains('类型')>

    <#if field.comment!?length gt 0>
        <#if springdoc>
    @Schema(description = "${field.comment}")
        <#elseif swagger>
    @ApiModelProperty("${field.comment}")
        <#else>
    /**
     * ${field.comment}
     */
        </#if>
    </#if>
    private ${field.propertyType} ${field.propertyName};
    </#if>
</#list>
<#------------  END 字段循环遍历  ---------->
<#elseif fileComment?contains("添加") || fileComment?contains("更新")>
<#-- ----------  BEGIN 字段循环遍历  ---------->
<#list table.fields as field>
<#if field.propertyName!="createUser" && field.propertyName!="createTime" && field.propertyName!="updateUser" && field.propertyName!="updateTime" && field.propertyName!="delFlag">
    <#if fileComment?contains("添加") && field.propertyName=="id">
    <#------- 添加时无主键 ------>
    <#else>

    <#if field.comment!?length gt 0>
        <#if springdoc>
    @Schema(description = "${field.comment}")
        <#elseif swagger>
    @ApiModelProperty("${field.comment}")
        <#else>
    /**
     * ${field.comment}
     */
        </#if>
    </#if>
    <#if field.columnType == "INTEGER">
    @NotNull(message = "${field.comment}不能为空")
    <#elseif field.columnType == "STRING">
    @NotBlank(message = "${field.comment}不能为空")
    </#if>
    private ${field.propertyType} ${field.propertyName};
    </#if>
</#if>
</#list>
<#------------  END 字段循环遍历  ---------->
</#if>

}
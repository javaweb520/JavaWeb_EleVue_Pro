package com.xiaomayi.monitor;

import de.codecentric.boot.admin.server.config.EnableAdminServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableAdminServer
@SpringBootApplication
public class MonitorApplication {
    public static void main(String[] args) {
        SpringApplication.run(MonitorApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  小蚂蚁云监控服务启动成功   ლ(´ڡ`ლ)ﾞ");
    }
}
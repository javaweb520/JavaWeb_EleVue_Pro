package com.xiaomayi.admin.service.impl;

import com.xiaomayi.admin.service.IndexService;
import com.xiaomayi.core.utils.R;
import com.xiaomayi.core.utils.StringUtils;
import com.xiaomayi.security.utils.SecurityUtils;
import com.xiaomayi.system.dto.user.UserUpdatePwdDTO;
import com.xiaomayi.system.entity.User;
import com.xiaomayi.system.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户 服务类
 * </p>
 *
 * @author 小蚂蚁团队
 * @since 2024-03-23
 */
@Service
@AllArgsConstructor
public class IndexServiceImpl implements IndexService {

    private final UserService userService;

    /**
     * 更新用户密码
     *
     * @param userUpdatePwdDTO 参数
     * @return 返回结果
     */
    @Override
    public R updatePassword(UserUpdatePwdDTO userUpdatePwdDTO) {
        // 原始密码
        String password = userUpdatePwdDTO.getPassword();
        // 用户ID
        Integer userId = SecurityUtils.getUserId();
        // 查询登录用户
        User user = userService.getInfo(userId);
        if (StringUtils.isNull(user)) {
            return R.failed("用户不存在");
        }
        // 验证原始密码是否正确
        if (!SecurityUtils.matchesPassword(user.getSalt() + password, user.getPassword())) {
            return R.failed("原始密码不正确");
        }
        // 验证新密码是否一致
        if (!userUpdatePwdDTO.getNewPassword().equalsIgnoreCase(userUpdatePwdDTO.getConfirmPassword())) {
            return R.failed("输入的新密码不一致");
        }
        // 判断原始密码是否与新密码相同
        if (password.equalsIgnoreCase(userUpdatePwdDTO.getNewPassword())) {
            return R.failed("新密码不能与原始密码相同");
        }
        // 设置新密码
        user.setPassword(SecurityUtils.encryptPassword(user.getSalt() + userUpdatePwdDTO.getNewPassword()));
        // 保存新密码
        boolean result = userService.updateById(user);
        if (!result) {
            return R.failed();
        }
        return R.ok();
    }
}
